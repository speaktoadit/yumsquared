package com.mycompany.EntityBeans;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2017-12-09T22:48:51")
@StaticMetamodel(Restaurant.class)
public class Restaurant_ { 

    public static volatile SingularAttribute<Restaurant, String> address;
    public static volatile SingularAttribute<Restaurant, String> featuredImage;
    public static volatile SingularAttribute<Restaurant, String> menuUrl;
    public static volatile SingularAttribute<Restaurant, String> name;
    public static volatile SingularAttribute<Restaurant, String> votes;
    public static volatile SingularAttribute<Restaurant, Integer> id;
    public static volatile SingularAttribute<Restaurant, String> aggregateRating;
    public static volatile SingularAttribute<Restaurant, String> priceRange;

}