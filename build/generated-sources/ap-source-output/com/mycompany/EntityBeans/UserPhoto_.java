package com.mycompany.EntityBeans;

import com.mycompany.EntityBeans.User;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2017-12-09T22:48:51")
@StaticMetamodel(UserPhoto.class)
public class UserPhoto_ { 

    public static volatile SingularAttribute<UserPhoto, String> extension;
    public static volatile SingularAttribute<UserPhoto, Integer> id;
    public static volatile SingularAttribute<UserPhoto, User> userId;

}