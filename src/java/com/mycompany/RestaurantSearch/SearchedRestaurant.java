/*
 * Created by Adit Parikh on 2017.10.21  * 
 * Copyright © 2017 Adit Parikh. All rights reserved. * 
 */
package com.mycompany.RestaurantSearch;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author adit
 */
public class SearchedRestaurant {

    private String res_id;
    private String name;
    private String url;
    private String address;
    private String latitude;
    private String longitude;
    private String cuisines;
    private String average_cost_for_two;
    private String price_range;
    private String aggregate_rating;
    private String votes;
    private String menu_url;
    private List<SearchedReview> reviews;

    public SearchedRestaurant() {
    }

    public SearchedRestaurant(String res_id, String name, String location, String address, String latitude, String longitude, String cuisines, String average_cost_for_two, String price_range, String aggregate_rating, String votes, String menu_url) {
        this.res_id = res_id;
        this.name = name;
        this.url = location;
        this.address = address;
        this.latitude = latitude;
        this.longitude = longitude;
        this.cuisines = cuisines;
        this.average_cost_for_two = average_cost_for_two;
        this.price_range = price_range;
        this.aggregate_rating = aggregate_rating;
        this.votes = votes;
        this.menu_url = menu_url;
    }

    public String getRes_id() {
        return res_id;
    }

    public void setRes_id(String res_id) {
        this.res_id = res_id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String location) {
        this.url = location;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getLatitude() {
        return latitude;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    public String getLongitude() {
        return longitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

    public String getCuisines() {
        return cuisines;
    }

    public void setCuisines(String cuisines) {
        this.cuisines = cuisines;
    }

    public String getAverage_cost_for_two() {
        return average_cost_for_two;
    }

    public void setAverage_cost_for_two(String average_cost_for_two) {
        this.average_cost_for_two = average_cost_for_two;
    }

    public String getPrice_range() {
        return price_range;
    }

    public void setPrice_range(String price_range) {
        this.price_range = price_range;
    }

    public String getAggregate_rating() {
        return aggregate_rating;
    }

    public void setAggregate_rating(String aggregate_rating) {
        this.aggregate_rating = aggregate_rating;
    }

    public String getVotes() {
        return votes;
    }

    public void setVotes(String votes) {
        this.votes = votes;
    }

    public String getMenu_url() {
        return menu_url;
    }

    public void setMenu_url(String menu_url) {
        this.menu_url = menu_url;
    }

    public List<SearchedReview> getReviews() {
        return reviews;
    }

    public void setReviews(List<SearchedReview> reviews) {
        this.reviews = reviews;
    }
}
