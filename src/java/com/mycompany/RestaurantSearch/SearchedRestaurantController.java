/*
 * Created by Adit Parikh on 2017.10.21  * 
 * Copyright © 2017 Adit Parikh. All rights reserved. * 
 */
package com.mycompany.RestaurantSearch;

import com.mycompany.managers.AccountManager;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Serializable;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.enterprise.context.SessionScoped;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.inject.Named;
import org.primefaces.json.JSONArray;
import org.primefaces.json.JSONObject;

/**
 *
 * @author adit
 */
@Named("searchedRestaurantController")
@SessionScoped

public class SearchedRestaurantController implements Serializable {

    @Inject
    AccountManager accountManager;

    private final String apiKey = "9090558692dd4c78024f31270e615627";
    private final String googleGeocodeApi = "&key=AIzaSyDtYo5JHXboSgrzsTP9tOlzROdiJ5EFwE0";

    List<SearchedRestaurant> searchedRestaurants;

    private String searchQuery;
    private String radius = "16093"; //10 mile default 

    private String convertRadius(String radius) {
        int intRadius = Integer.parseInt(radius);

        //convert from miles to meters
        intRadius = intRadius * 1609;

        radius = String.valueOf(intRadius);
        return radius;
    }

    private SearchedRestaurant selected;
    private String statusMessage;

    private final String zomatoUrl1 = "https://developers.zomato.com/api/v2.1/search?entity_type=zone&q=";
    String userLatitude = "38.9586"; //default Reston, VA
    String userLongitude = "-77.3570"; //default Reston VA
    private final String zomatoUrl2 = "&count=100&lat=";
    private final String zomatoUrl4 = "&radius=";

    private final String googleGeocodeUrlBase = "https://maps.googleapis.com/maps/api/geocode/json?address=";

    /**
     *
     * @return
     */
    public String performSearch() throws MalformedURLException, IOException {
        if (accountManager.isLoggedIn()) {
            radius = convertRadius(radius);

            //System.out.println(googleGeocodeUrlBase + accountManager.getSelected().getZipcode() + googleGeocodeApi);
            JSONArray locationArray;
            try {
                String userLocationResults = readUrlContent(googleGeocodeUrlBase + accountManager.getSelected().getZipcode() + googleGeocodeApi);
                userLocationResults = "[" + userLocationResults + "]";
                locationArray = new JSONArray(userLocationResults);
                JSONArray jsonArrayFoundLocation = locationArray.getJSONObject(0).getJSONArray("results");
                JSONObject locationObject = jsonArrayFoundLocation.getJSONObject(0);
                String coordinates = locationObject.optString("geometry", "");
                if (coordinates.equals("")) {
                    coordinates = "No Location Provided!";
                }

                JSONObject locObj = new JSONObject(coordinates);

                String cor = locObj.optString("location", "");
                if (coordinates.equals("")) {
                    coordinates = "No Location Provided!";
                }

                JSONObject locObj2 = new JSONObject(cor);
                String lat = locObj2.optString("lat", "");
                if (lat.equals("")) {
                    lat = "No Latitude Provided!";
                }
                String lon = locObj2.optString("lng", "");
                if (lon.equals("")) {
                    lon = "No Longitude Provided!";
                }
                userLatitude = lat;
                userLongitude = lon;

                //System.out.println("lat" + lat + "lon" + lon);
            } catch (Exception ex) {
                statusMessage = ex.getMessage();
                Logger.getLogger(SearchedRestaurantController.class.getName()).log(Level.SEVERE, null, ex);
            }
        }

        String zomatoUrl3 = userLatitude + "&lon=" + userLongitude;

        selected = null;
        statusMessage = "";

        JSONArray jsonArray;
        searchedRestaurants = new ArrayList();

        searchQuery = searchQuery.replaceAll(" ", "%20");

        String zomatoWebServiceRequestURL = zomatoUrl1 + searchQuery + zomatoUrl2 + zomatoUrl3 + zomatoUrl4 + radius;
        //System.out.println(zomatoWebServiceRequestURL);

        try {
            String jsonDataObtained = readUrlContent(zomatoWebServiceRequestURL);

            jsonDataObtained = "[" + jsonDataObtained + "]";

            jsonArray = new JSONArray(jsonDataObtained);

            JSONArray restaurantsArray = jsonArray.getJSONObject(0).getJSONArray("restaurants");

            int index = 0;
            if (restaurantsArray.length() > index) {
                while (restaurantsArray.length() > index) {
                    JSONObject jsonObject = restaurantsArray.getJSONObject(index);
                    JSONObject restaurantObject = jsonObject.getJSONObject("restaurant");

                    String res_id = restaurantObject.optString("id", "");
                    if (res_id.equals("")) {
                        res_id = "No res_id!";
                    }

                    String name = restaurantObject.optString("name", "");
                    if (name.equals("")) {
                        name = "No name!";
                    }

                    String url = restaurantObject.optString("url", "");
                    if (url.equals("")) {
                        url = "No url!";
                    }

                    JSONObject locationObject = restaurantObject.getJSONObject("location");

                    String address = locationObject.optString("address", "");
                    if (address.equals("")) {
                        address = "No address!";
                    }

                    String latitude = locationObject.optString("latitude", "");
                    if (latitude.equals("")) {
                        latitude = "No latitude!";
                    }

                    String longitude = locationObject.optString("longitude", "");
                    if (longitude.equals("")) {
                        longitude = "No longitude!";
                    }

                    String cuisines = restaurantObject.optString("cuisines", "");
                    if (cuisines.equals("")) {
                        cuisines = "No cuisines!";
                    }

                    String average_cost_for_two = restaurantObject.optString("average_cost_for_two", "");
                    if (average_cost_for_two.equals("")) {
                        average_cost_for_two = "No average_cost_for_two!";
                    }

                    String price_range = restaurantObject.optString("price_range", "");
                    if (price_range.equals("")) {
                        price_range = "No price_range!";
                    }

                    JSONObject userRatingObject = restaurantObject.getJSONObject("user_rating");

                    String aggregate_rating = userRatingObject.optString("aggregate_rating", "");
                    if (aggregate_rating.equals("")) {
                        aggregate_rating = "No aggregate_rating!";
                    }

                    String votes = userRatingObject.optString("votes", "");
                    if (votes.equals("")) {
                        votes = "No votes!";
                    }

                    String menu_url = restaurantObject.optString("menu_url", "");
                    if (menu_url.equals("")) {
                        menu_url = "No menu_url!";
                    }

                    SearchedRestaurant restaurant = new SearchedRestaurant(res_id,
                            name, votes, address, latitude, longitude, cuisines,
                            average_cost_for_two, price_range, aggregate_rating,
                            votes, menu_url);

                    populateReviews(restaurant);

                    searchedRestaurants.add(restaurant);
                    index++;
                }
            } else {
                FacesContext.getCurrentInstance().addMessage(null, new FacesMessage("No restaurant found for the search query!"));
                FacesContext.getCurrentInstance().getExternalContext().getFlash().setKeepMessages(true);
            }
        } catch (Exception ex) {
            statusMessage = ex.getMessage();
            Logger.getLogger(SearchedRestaurantController.class.getName()).log(Level.SEVERE, null, ex);
        }

        return "RestaurantSearchResults?faces-redirect=true";
    }

    /**
     *
     * @param restaurant
     */
    public void populateReviews(SearchedRestaurant restaurant) throws Exception {
        String reviewUrlBase = "https://developers.zomato.com/api/v2.1/reviews?res_id=";
        String res_id = restaurant.getRes_id();
        try {
            String jsonDataObtained = readUrlContent(reviewUrlBase + res_id);
            jsonDataObtained = "[" + jsonDataObtained + "]";
            JSONArray jsonArray = new JSONArray(jsonDataObtained);
            JSONArray reviewsArray = jsonArray.getJSONObject(0).getJSONArray("user_reviews");
            int index = 0;
            if (reviewsArray.length() > index) {
                while (reviewsArray.length() > index) {
                    JSONObject jsonObject = reviewsArray.getJSONObject(index);
                    JSONObject reviewObject = jsonObject.getJSONObject("review");

                    String rating = reviewObject.optString("rating", "");
                    if (rating.equals("")) {
                        rating = "No rating!";
                    }

                    String review_text = reviewObject.optString("review_text", "");
                    if (review_text.equals("")) {
                        rating = "No review text!";
                    }
                    String id = reviewObject.optString("id", "");
                    if (id.equals("")) {
                        rating = "No id!";
                    }

                    JSONObject userObject = reviewObject.getJSONObject("user");

                    String name = userObject.optString("name", "");
                    if (name.equals("")) {
                        rating = "No name!";
                    }

                    List<SearchedReview> searchedReviews = new ArrayList();
                    SearchedReview searchedReview = new SearchedReview(rating, review_text, id, name, res_id);
                    searchedReviews.add(searchedReview);

                    restaurant.setReviews(searchedReviews);

                    System.out.println(searchedReview.getReview_text());
                    index++;
                }
            } else {
            }

        } catch (Exception ex) {
            statusMessage = ex.getMessage();
            Logger
                    .getLogger(SearchedRestaurantController.class
                            .getName()).log(Level.SEVERE, null, ex);
        }

        //System.out.println(reviewsArray.toString());
    }

    /**
     *
     * @param webServiceUrl
     * @return
     * @throws java.lang.Exception
     */
    public String readUrlContent(String webServiceUrl) throws Exception {
        BufferedReader reader = null;
        try {
            URL url = new URL(webServiceUrl);

            HttpURLConnection connection = (HttpURLConnection) url.openConnection();

            connection.setRequestMethod("GET");

            connection.setRequestProperty("Accept", "application/json");
            connection.setRequestProperty("user-key", apiKey);

            connection.connect();

            int status = connection.getResponseCode();
            System.out.println("Status: " + status);
            InputStream inputStream;

            if (status != HttpURLConnection.HTTP_OK) {
                inputStream = connection.getErrorStream();
            } else {
                inputStream = connection.getInputStream();
            }

            reader = new BufferedReader(new InputStreamReader(inputStream));
            StringBuilder buffer = new StringBuilder();
            char[] chars = new char[10240];
            int numberOfCharactersRead;
            while ((numberOfCharactersRead = reader.read(chars)) != -1) {
                buffer.append(chars, 0, numberOfCharactersRead);
            }
            return buffer.toString();
        } finally {
            if (reader != null) {
                reader.close();
            }
        }
    }

    public String readGeogodeUrl(String webServiceUrl) throws Exception {
        BufferedReader reader = null;
        try {
            URL url = new URL(webServiceUrl);
            reader = new BufferedReader(new InputStreamReader(url.openStream()));
            StringBuilder buffer = new StringBuilder();
            char[] chars = new char[10240];
            int numberOfCharactersRead;
            while ((numberOfCharactersRead = reader.read(chars)) != -1) {
                buffer.append(chars, 0, numberOfCharactersRead);
            }
            return buffer.toString();
        } finally {
            if (reader != null) {
                reader.close();
            }
        }
    }

    public List<SearchedRestaurant> getSearchedRestaurants() {
        return searchedRestaurants;
    }

    public void setSearchedRestaurants(List<SearchedRestaurant> searchedRestaurants) {
        this.searchedRestaurants = searchedRestaurants;
    }

    public String getSearchQuery() {
        return searchQuery;
    }

    public void setSearchQuery(String searchQuery) {
        this.searchQuery = searchQuery;
    }

    public SearchedRestaurant getSelected() {
        return selected;
    }

    public void setSelected(SearchedRestaurant selected) {
        this.selected = selected;
    }

    public String getStatusMessage() {
        return statusMessage;
    }

    public void setStatusMessage(String statusMessage) {
        this.statusMessage = statusMessage;
    }

    public String getRadius() {
        return radius;
    }

    public void setRadius(String radius) {
        this.radius = radius;
    }

    public AccountManager getAccountManager() {
        return accountManager;
    }

    public void setAccountManager(AccountManager accountManager) {
        this.accountManager = accountManager;
    }

    public String getUserLatitude() {
        return userLatitude;
    }

    public void setUserLatitude(String userLatitude) {
        this.userLatitude = userLatitude;
    }

    public String getUserLongitude() {
        return userLongitude;
    }

    public void setUserLongitude(String userLongitude) {
        this.userLongitude = userLongitude;
    }

}
