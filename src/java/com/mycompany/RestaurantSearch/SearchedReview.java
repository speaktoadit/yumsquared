/*
 * Created by Adit Parikh on 2017.12.05  * 
 * Copyright © 2017 Adit Parikh. All rights reserved. * 
 */
package com.mycompany.RestaurantSearch;

/**
 *
 * @author adit
 */
public class SearchedReview {
    private String rating;
    private String review_text;
    private String id;
    private String name;
    private String res_id;

    public SearchedReview(String rating, String review_text, String id, String name, String res_id) {
        this.rating = rating;
        this.review_text = review_text;
        this.id = id;
        this.name = name;
        this.res_id = res_id;
    }

    public String getRating() {
        return rating;
    }

    public void setRating(String rating) {
        this.rating = rating;
    }

    public String getReview_text() {
        return review_text;
    }

    public void setReview_text(String review_text) {
        this.review_text = review_text;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getRes_id() {
        return res_id;
    }

    public void setRes_id(String res_id) {
        this.res_id = res_id;
    }
}
